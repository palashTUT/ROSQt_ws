#!/usr/bin/env python

import rospy
from moveit_commander import MoveGroupCommander
from actionlib_msgs.msg import GoalStatusArray
targets = [[0.016610232681035994, -1.253123052052089, 0.08541428865705218, -2.268844215938023, 0.09507987835577555, 1.018526998417718, 1.0064559217861722],
        [0.001008606319847916, -0.7908328604016985, 0.07558093907151904, -1.4646298473903112, 0.0856165361404419, 0.6770353179318565, 1.0334249186515807],
        [-0.026000336649162428, -0.464988059929439, 0.02804530129262379, -2.1000964298248292, -0.018652211661849702, 1.8726339648451125, 0.9170354574067252],
        [-0.19080302163532803, -0.3714338301931109, -0.17932583262239185, -1.920874613761902, -0.09081563834633145, 1.4459725790364402, 0.8940264429364886],
        [-0.0009132466006891004, 0.061491101452282496, 0.05188103946404798, -1.5261891600745066, -0.0392253012742315, 1.5730987216745105, 0.8523099657467433],
        [0.46566397094726564, -0.5310044989585877, 0.9598920833723885, -1.9162216776439123, 0.4280036331500326, 1.5891122554029737, 0.5804895615577698],
        [-0.3614412453515189, -0.15590985657487597, 0.7923782605103084, -1.245281703540257, 0.12212543249130249, 1.137271715062005, 0.7233048292568752],
        [-9.098391741281376e-05, -0.7852028447559901, 6.919532831359132e-06, -2.355915191922869, 3.772705219619508e-05, 1.570735102551324, 0.7848721194267273],
        [-0.0002259312839347071, -0.4301715797356197, -0.0007989248161736343, -0.5623078370775495, 0.0006690844224898943, 0.1324893149307796, 0.7849910092353821]
        ]

file = open('poses.txt','w')

if __name__ == '__main__':
    rospy.init_node('move_to_start')
    rospy.wait_for_message('move_group/status', GoalStatusArray)
    commander = MoveGroupCommander('panda_arm')
    print(commander.get_current_pose())
    # #commander.set_named_target('ready')
    # #commander.go()
    # #commander.go(wait=True)
    #
    # for target in targets:
    #     file.write('------------------------------[NEW POSE]---------------------------------' + '\n')
    #     commander.set_joint_value_target(target)
    #     commander.go(wait=True)
    #     file.write(str(commander.get_current_joint_values()))
    #     file.write('\n')
    #     file.write(str(commander.get_current_pose().pose))
    #     file.write('\n')
    #     file.write('------------------------------[/NEW POSE]--------------------------------' + '\n\n')
    # file.close()
    #     #print(commander.get_current_pose())
    """
    print(commander.get_current_joint_values())
    file.write('position 1:' + '\n')
    file.write(str(commander.get_current_joint_values()))
    file.write('\n')
    print(commander.get_current_pose())
    file.write(str(commander.get_current_pose().pose))
    file.close()
    #pose = commander.get_current_pose()#.pose.position.x = 0
    #pose.pose.position.x=0
    #commander.set_pose_target(pose)
    #commander.go(wait=True)
    """

